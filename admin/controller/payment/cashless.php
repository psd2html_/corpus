<?php

class ControllerPaymentCashless extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('payment/cashless');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('cashless', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_all_zones'] = $this->language->get('text_all_zones');

        $data['entry_bank'] = $this->language->get('entry_bank');
        $data['entry_total'] = $this->language->get('entry_total');
        $data['entry_order_status'] = $this->language->get('entry_order_status');
        $data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_beneficiary'] = $this->language->get('entry_beneficiary');
        $data['entry_bin'] = $this->language->get('entry_bin');
        $data['entry_bankname'] = $this->language->get('entry_bankname');
        $data['entry_bik'] = $this->language->get('entry_bik');
        $data['entry_iik'] = $this->language->get('entry_iik');
        $data['entry_knp'] = $this->language->get('entry_knp');
        $data['entry_kbe'] = $this->language->get('entry_kbe');
        $data['entry_address'] = $this->language->get('entry_address');
        $data['entry_contact'] = $this->language->get('entry_contact');

        $data['help_total'] = $this->language->get('help_total');
        $data['help_data_input'] = $this->language->get('help_data_input');

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_details'] = $this->language->get('tab_details');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();

        foreach ($languages as $language) {
            if (isset($this->error['bank' . $language['language_id']])) {
                $data['error_bank' . $language['language_id']] = $this->error['bank' . $language['language_id']];
            } else {
                $data['error_bank' . $language['language_id']] = '';
            }
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_payment'),
            'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('payment/cashless', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['action'] = $this->url->link('payment/cashless', 'token=' . $this->session->data['token'], 'SSL');

        $data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

        $this->load->model('localisation/language');

        foreach ($languages as $language) {
            if (isset($this->request->post['cashless_bank' . $language['language_id']])) {
                $data['cashless_bank' . $language['language_id']] = $this->request->post['cashless_bank' . $language['language_id']];
            } else {
                $data['cashless_bank' . $language['language_id']] = $this->config->get('cashless_bank' . $language['language_id']);
            }
        }

        $data['languages'] = $languages;

        if (isset($this->request->post['cashless_total'])) {
            $data['cashless_total'] = $this->request->post['cashless_total'];
        } else {
            $data['cashless_total'] = $this->config->get('cashless_total');
        }

        if (isset($this->request->post['cashless_order_status_id'])) {
            $data['cashless_order_status_id'] = $this->request->post['cashless_order_status_id'];
        } else {
            $data['cashless_order_status_id'] = $this->config->get('cashless_order_status_id');
        }

        $this->load->model('localisation/order_status');

        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (isset($this->request->post['cashless_geo_zone_id'])) {
            $data['cashless_geo_zone_id'] = $this->request->post['cashless_geo_zone_id'];
        } else {
            $data['cashless_geo_zone_id'] = $this->config->get('cashless_geo_zone_id');
        }

        $this->load->model('localisation/geo_zone');

        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        if (isset($this->request->post['cashless_status'])) {
            $data['cashless_status'] = $this->request->post['cashless_status'];
        } else {
            $data['cashless_status'] = $this->config->get('cashless_status');
        }

        if (isset($this->request->post['cashless_sort_order'])) {
            $data['cashless_sort_order'] = $this->request->post['cashless_sort_order'];
        } else {
            $data['cashless_sort_order'] = $this->config->get('cashless_sort_order');
        }

        if (isset($this->request->post['cashless_beneficiary'])) {
            $data['cashless_beneficiary'] = $this->request->post['cashless_beneficiary'];
        } else {
            $data['cashless_beneficiary'] = $this->config->get('cashless_beneficiary');
        }

        if (isset($this->request->post['cashless_bin'])) {
            $data['cashless_bin'] = $this->request->post['cashless_bin'];
        } else {
            $data['cashless_bin'] = $this->config->get('cashless_bin');
        }

        if (isset($this->request->post['cashless_bankname'])) {
            $data['cashless_bankname'] = $this->request->post['cashless_bankname'];
        } else {
            $data['cashless_bankname'] = $this->config->get('cashless_bankname');
        }

        if (isset($this->request->post['cashless_bik'])) {
            $data['cashless_bik'] = $this->request->post['cashless_bik'];
        } else {
            $data['cashless_bik'] = $this->config->get('cashless_bik');
        }

        if (isset($this->request->post['cashless_iik'])) {
            $data['cashless_iik'] = $this->request->post['cashless_iik'];
        } else {
            $data['cashless_iik'] = $this->config->get('cashless_iik');
        }

        if (isset($this->request->post['cashless_knp'])) {
            $data['cashless_knp'] = $this->request->post['cashless_knp'];
        } else {
            $data['cashless_knp'] = $this->config->get('cashless_knp');
        }

        if (isset($this->request->post['cashless_kbe'])) {
            $data['cashless_kbe'] = $this->request->post['cashless_kbe'];
        } else {
            $data['cashless_kbe'] = $this->config->get('cashless_kbe');
        }

        if (isset($this->request->post['cashless_address'])) {
            $data['cashless_address'] = $this->request->post['cashless_address'];
        } else {
            $data['cashless_address'] = $this->config->get('cashless_address');
        }

        if (isset($this->request->post['cashless_contact'])) {
            $data['cashless_contact'] = $this->request->post['cashless_contact'];
        } else {
            $data['cashless_contact'] = $this->config->get('cashless_contact');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('payment/cashless.tpl', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'payment/cashless')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();

        foreach ($languages as $language) {
            if (empty($this->request->post['cashless_bank' . $language['language_id']])) {
                $this->error['bank' . $language['language_id']] = $this->language->get('error_bank');
            }
        }

        return !$this->error;
    }

}
