<?php
// Heading
$_['heading_title']    = 'TLT Blog - Tags Module';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified TLT Blog - Tags Module!';
$_['text_edit']        = 'Edit TLT Blog - Tags Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify TLT Blog - Tags module!';