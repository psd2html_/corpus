<?php
// Heading
$_['heading_title']    	= 'TLT Blog Settings';

// Text
$_['text_module']      	= 'Modules';
$_['text_success']     	= 'Success: You have modified TLT Blog settings!';
$_['text_edit']        	= 'Settings';

// Entry
$_['entry_path']        = 'Path';
$_['entry_path_title']  = 'Path Title';
$_['entry_show_path']	= 'Show Path';
$_['entry_num_columns']	= 'Columns';
$_['entry_show_image']	= 'Show Image';
$_['entry_width']      	= 'Width';
$_['entry_height']     	= 'Height';
$_['entry_seo']     	= 'TLT Blog SEO Url';
$_['entry_status']     	= 'Status';

//Help
$_['help_path']         = 'Name of global path name in URL for blogs (i.e. blogs). Do not use spaces, instead replace spaces with -.';
$_['help_path_title']   = 'Localized title for path, which will be shown in breadcrumbs and headers.';
$_['help_show_path']    = 'Show path in breadcrumbs';
$_['help_show_image']   = 'Show image of blog entry in blog listings';
$_['help_num_columns']  = 'Number of columns in blog listing';
$_['help_seo']  		= 'Enable TLT Blog SEO module. <b>You have to change your SEO Module!</b> Read README.TXT for more info.';
$_['help_status']  		= 'Globall On/Off blog functionality on front-end';

// Error
$_['error_permission'] 	= 'Warning: You do not have permission to modify TLT Blog settings!';
$_['error_path']        = 'Path must be between 3 and 64 characters!';
$_['error_path_exist']  = 'This URL already exists!';
$_['error_path_title']  = 'Path title must be between 3 and 64 characters!';
$_['error_width']      	= 'Width required!';
$_['error_height']     	= 'Height required!';
