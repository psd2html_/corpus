<?php
// Heading
$_['heading_title']    	= 'TLT Blog Module';

// Text
$_['text_module']      	= 'Modules';
$_['text_success']     	= 'Success: You have modified TLT Blog Module!';
$_['text_edit']        	= 'Edit TLT Blog Module';
$_['text_sortorder']    = 'By Sort Order';
$_['text_random']       = 'Random';

// Entry
$_['entry_name']        = 'Module Name';
$_['entry_title']      	= 'Title';
$_['entry_show_title']	= 'Show Title';
$_['entry_show_blogs']	= 'Link to the description';
$_['entry_limit']      	= 'Limit';
$_['entry_sort']      	= 'Sort Order';
$_['entry_num_columns']	= 'Columns';
$_['entry_show_image']	= 'Show Image';
$_['entry_width']      	= 'Width';
$_['entry_height']     	= 'Height';
$_['entry_tags_to_show'] = 'Tags to show';
$_['entry_template']    = 'Template';
$_['entry_status']     	= 'Status';

// Help
$_['help_sort']      	= 'Random will work if limit less than total count of blog entries.';
$_['help_show_blogs']   = 'Create active links to the full description or show only title and introduction.';
$_['help_limit']   		= 'How many blog entries show.';
$_['help_template'] 	= 'Template for front-end. Template file should be in modules folder of active tempale.';
$_['help_tags_to_show'] = 'If empty - show from all tags.';

// Error
$_['error_permission'] 	= 'Warning: You do not have permission to modify TLT Blog Module!';
$_['error_name']        = 'Module Name must be between 3 and 64 characters!';
$_['error_title']       = 'Module Title must be between 3 and 64 characters!';
$_['error_limit']      	= 'Limit required!';
$_['error_width']      	= 'Width required!';
$_['error_height']     	= 'Height required!';
$_['error_template']    = 'Template required!';