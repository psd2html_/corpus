<?php

// Heading
$_['heading_title'] = 'Безналичный платеж';

// Text
$_['text_payment'] = 'Оплата';
$_['text_success'] = 'Настройки успешно изменены!';
$_['text_edit'] = 'Редактирование';
$_['text_cashless'] = '<img src="view/image/payment/pdf.png" alt="Печать счета в pdf" title="Печать счета в pdf" style="border: 1px solid #EEEEEE; height: 40px;" />';

// Entry
$_['entry_bank'] = 'Инструкция по переводу средств';
$_['entry_total'] = 'Нижняя граница';
$_['entry_order_status'] = 'Статус заказа после оплаты';
$_['entry_geo_zone'] = 'Географическая зона';
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Порядок сортировки';
$_['entry_beneficiary'] = 'Бенефициар';
$_['entry_bin'] = 'БИН/ИИН';
$_['entry_bankname'] = 'Банк бенефициара';
$_['entry_bik'] = 'БИК';
$_['entry_iik'] = 'ИИК';
$_['entry_kbe'] = 'Кбе';
$_['entry_knp'] = 'Код назначения платежа';
$_['entry_address'] = 'Юридический адрес';
$_['entry_contact'] = 'Телефон/Факс/E-mail';

// Help
$_['help_total'] = 'Минимальная сумма заказа. Ниже данной суммы, способ оплаты будет недоступен.';
$_['help_data_input']              = 'Введите значение';

// Error
$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';
$_['error_bank'] = 'Необходимо заполнить инструкцию по переводу!';

// Tab
$_['tab_details']      = 'Реквизиты';
