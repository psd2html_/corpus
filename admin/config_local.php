<?php
// HTTP
define('HTTP_SERVER', 'http://corpus.local/admin/');
define('HTTP_CATALOG', 'http://corpus.local/');

// HTTPS
define('HTTPS_SERVER', 'http://corpus.local/admin/');
define('HTTPS_CATALOG', 'http://corpus.local/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'corpus_project');
define('DB_PASSWORD', 'gUxy8piM');
define('DB_DATABASE', 'corpus');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');