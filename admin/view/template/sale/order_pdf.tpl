<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
</head>
<body>
<div class="container">
<div class="top">
  <table style="width:100%;">
    <tbody>
      <tr>
        <td style="width:60%;">
          <img src="logo.jpg" alt="">
        </td>
        <td style="width:40%;">
          <p>Реквiзити магазину:</p>
        </td>
      </tr>
    </tbody>
  </table>
</div>
  <?php foreach ($orders as $order) { ?>
  <div style="page-break-after: always;">
    <h1><?php echo $text_invoice; ?> #<?php echo $order['order_id']; ?></h1>
    <table class="table table-bordered" style="border:2px solid #000000;">
      <thead>
        <tr>
          <td colspan="2"><?php echo $text_order_detail; ?></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width: 50%;border:2px solid #000000;"><address>
            <strong><?php echo $order['store_name']; ?></strong><br />
            <?php echo $order['store_address']; ?>
            </address>
            <b><?php echo $text_telephone; ?></b> <?php echo $order['store_telephone']; ?><br />
            <?php if ($order['store_fax']) { ?>
            <b><?php echo $text_fax; ?></b> <?php echo $order['store_fax']; ?><br />
            <?php } ?>
            <b><?php echo $text_email; ?></b> <?php echo $order['store_email']; ?><br />
            <b><?php echo $text_website; ?></b> <a href="<?php echo $order['store_url']; ?>"><?php echo $order['store_url']; ?></a></td>
          <td style="width: 50%;border:2px solid #000000;"><b><?php echo $text_date_added; ?></b> <?php echo $order['date_added']; ?><br />
            <?php if ($order['invoice_no']) { ?>
            <b><?php echo $text_invoice_no; ?></b> <?php echo $order['invoice_no']; ?><br />
            <?php } ?>
            <b><?php echo $text_order_id; ?></b> <?php echo $order['order_id']; ?><br />
            <b><?php echo $text_payment_method; ?></b> <?php echo $order['payment_method']; ?><br />
            <?php if ($order['shipping_method']) { ?>
            <b><?php echo $text_shipping_method; ?></b> <?php echo $order['shipping_method']; ?><br />
            <?php } ?></td>
        </tr>
      </tbody>
    </table>
    <table class="table table-bordered" style="border:2px solid #000000;">
      <thead>
        <tr>
          <td style="width: 50%;border:2px solid #000000;"><b><?php echo $text_to; ?></b></td>
          <td style="width: 50%;border:2px solid #000000;"><b><?php echo $text_ship_to; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="border:2px solid #000000;"><address>
            <?php echo $order['payment_address']; ?>
            </address></td>
          <td style="border:2px solid #000000;"><address>
            <?php echo $order['shipping_address']; ?>
            </address></td>
        </tr>
      </tbody>
    </table>
    <table class="table table-bordered" style="border:2px solid #000000;">
      <thead>
        <tr>
          <td style="border:2px solid #000000;"><b><?php echo $column_product; ?></b></td>
          <td style="border:2px solid #000000;"><b><?php echo $column_model; ?></b></td>
          <td class="text-right" style="border:2px solid #000000;"><b><?php echo $column_quantity; ?></b></td>
          <td class="text-right" style="border:2px solid #000000;"><b><?php echo $column_price; ?></b></td>
          <td class="text-right" style="border:2px solid #000000;"><b><?php echo $column_total; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr>
          <td style="border:2px solid #000000;"><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
          <td style="border:2px solid #000000;"><?php echo $product['model']; ?></td>
          <td class="text-right" style="border:2px solid #000000;"><?php echo $product['quantity']; ?></td>
          <td class="text-right" style="border:2px solid #000000;"><?php echo $product['price']; ?></td>
          <td class="text-right" style="border:2px solid #000000;"><?php echo $product['total']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['voucher'] as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td class="text-right">1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['total'] as $total) { ?>
        <tr>
          <td class="text-right" colspan="4"><b><?php echo $total['title']; ?></b></td>
          <td class="text-right"><?php echo $total['text']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php if ($order['comment']) { ?>
    <table class="table table-bordered" style="border:2px solid #000000;">
      <thead>
        <tr>
          <td style="border:2px solid #000000;"><b><?php echo $column_comment; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="border:2px solid #000000;"><?php echo $order['comment']; ?></td>
        </tr>
      </tbody>
    </table>
    <?php } ?>
  </div>
  <?php } ?>
</div>
</body>
</html>