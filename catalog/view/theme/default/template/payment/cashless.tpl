<h2><?php echo $text_instruction; ?></h2>

<div class="well well-sm">
    <p><b><?php echo $text_description; ?></b></p>
    <div class="pull-right">
        <a href="<?php echo $print; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $text_print_invoice_order; ?>" class="btn btn-primary"><i class="fa fa-print"></i></a>
    </div>
    <p><?php echo $bank; ?></p>
    <p><?php echo $text_payment; ?></p>
</div>
<div class="buttons">
    <div class="pull-right">
        <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-primary" data-loading-text="<?php echo $text_loading; ?>" />
    </div>
</div>
<script type="text/javascript"><!--
$('#button-confirm').on('click', function () {
        $.ajax({
            type: 'get',
            url: 'index.php?route=payment/cashless/confirm',
            cache: false,
            beforeSend: function () {
                $('#button-confirm').button('loading');
            },
            complete: function () {
                $('#button-confirm').button('reset');
            },
            success: function () {
                location = '<?php echo $continue; ?>';
            }
        });
    });
//--></script>
