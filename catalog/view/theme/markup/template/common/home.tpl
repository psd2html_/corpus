<?php echo $header; ?>

  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class=""><?php echo $content_top; ?>
    <?php if ($categories) { ?>
    <div class="produkt-holder">
                <div class="ttl-01">Продукція</div>
                <ul>
                <?php foreach($categories as $cat): ?>
                    <li <?php if($cat['icon'] == 1){ ?> class="ex" <?php } ?>>
                        <a href="<?php echo $cat['href']; ?>">
                        <div class="like_table"><div class="table_cell"> 
                        <?php if($cat['image'] != ''): ?>
                            <img src="/image/<?php echo $cat['image']; ?>" alt="image" class="img-responsive" style="max-width:256px;">
                        <?php else: ?>
                            <img src="/image/no_image.png" alt="image" class="img-responsive">    
                        <?php endif; ?>    
                        </div></div>
                            <span><?php echo $cat['name']; ?></span>
                        </a>
                    </li>
                    <?php endforeach; ?>
                    
                </ul>
            </div>
             <?php } ?>
    <?php echo $content_bottom; ?>     
    </div>   
    <div class="bottom-inf-main">
                <div class="about">
                    <?php echo $column_right; ?>
                </div>
                <?php if($posts): ?>
                <div class="last-post">
                    <div class="ttl-01">Останні записи</div>
                    <ul>
                        <?php foreach($posts as $post):?>
                        <li>
                            <a href="<?php echo $post['href']; ?>">
                                <div class="img" style="width:77px;">
                                    <img src="/image/<?php echo $post['image']; ?>" alt="image" style="width:67px;">
                                </div>
                                <div class="txt">
                                    <strong><?php echo $post['title']; ?></strong>
                                    <span><?php echo $post['intro']; ?></span>
                                </div>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </div>
                <?php endif; ?>
            </div>
    

<?php echo $footer; ?>