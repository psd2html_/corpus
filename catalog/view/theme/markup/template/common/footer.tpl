</div></div><!--Убрать этот тег-->
</div><!--Убрать этот тег-->

<footer id="footer">
  <div class="container">
    <div class="container-in">
      <div class="logo-holder">
        <strong class="logo"><a href="<?php echo $home; ?>">Corpus</a></strong>
        <div class="copy">© 2016 ТОВ "Корпус Груп". Всi права захищенi.</div>
      </div>
      <div class="soc-icon">
        <!--<ul>
          <li class="fc"><a href="#">facebook</a></li>
          <li class="in"><a href="#">linkedin</a></li>
          <li class=""><a href="#">twitter</a></li>
        </ul>-->
        <?php echo $footer_soc; ?>
      </div>
      <div class="menu-footer">
        <ul>
        <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <div class="dev">
        <a href="http://www.mgn.com.ua" target="_blank">Разработка сайта MGN</a>
      </div>
    </div>
  </div>
</footer><!-- / footer -->

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'Zrvvq0FCwg';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->

</body></html>