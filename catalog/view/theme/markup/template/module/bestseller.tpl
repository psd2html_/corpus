

<div class="top-produkt">
        <div class="ttl-01"><?php echo $heading_title; ?></div>
        <ul>
        <?php foreach ($products as $product) { ?>
          <li <?php if($product['icon']){ ?> class="ex" <?php } ?>>
            <a href="<?php echo $product['href']; ?>">
            <div class="like_table"><div class="table_cell">  
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
            </div></div>
            </a>
              <div class="bottom-inf">
                <span><?php echo $product['name']; ?></span>
                <div class="desc">
                  <?php echo $product['description']; ?>
                </div>
                <div>
                <?php if ($product['price']) { ?>
                  <div class="price"><?php echo $product['price']; ?>
                  </div>
                <?php } ?>  
                  <a href="<?php echo $product['href']; ?>" class="btn-cut">Замовити</a>
                </div>
            </div>
          </li>
          <?php } ?>
          
        </ul>
      </div>