<?php echo $header; ?>
<style>

</style>
<div class="baner" style="height:310px;">
        <?php echo $content_top; ?>
      </div>
      <div class="holder-sidebar">
        <div class="sidebar">
          <div class="ttl">КАТАЛОГ ПРОДУКЦІЇ</div>
          <?php if($categories): ?>
          <ul>
          <?php foreach($categories as $cat){ ?>
            <li><a href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a></li>
          <?php } ?>
          </ul>
          <?php endif; ?>
        </div>
        <div class="right-hold">
          <ul class="breadcrumbs">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php
                    if (($breadcrumb['text'] == 'Статьи') or ($breadcrumb['text'] == 'articles'))
                        $breadcrumb['text'] = 'Статтi';
                ?>
              <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
          <div class="statii-list">
             <?php if($tltblogs): ?> 
            <ul>
              <?php foreach ($tltblogs as $tltblog) { ?>
              <li>
                <a href="<?php echo $tltblog['href']; ?>">
                  <div class="img"><img src="<?php echo $tltblog['thumb']; ?>" alt="<?php echo $tltblog['title']; ?>" class="img-responsive"></div>
                  <div class="txt">
                    <div class="ttl"><?php echo $tltblog['title']; ?></div>
<!--                    <div class="date"><?php echo $tltblog['date']; ?></div>-->
                    <p><?php echo $tltblog['intro']; ?></p>
                  </div>
                </a>
              </li>
              <?php } ?>
            </ul>
            <?php endif; ?>  
          </div>
        </div>
      </div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.link2').addClass('active');
  })
</script>      
<?php echo $footer; ?>
