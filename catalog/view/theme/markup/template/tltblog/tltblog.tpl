<?php echo $header; ?>

<div class="baner" style="height:310px;">

<?php echo $content_top; ?>
      </div>
      <div class="holder-sidebar">
        <div class="sidebar">
          <div class="ttl">КАТАЛОГ ПРОДУКЦІЇ</div>
          <?php if($categories): ?>
          <ul>
          <?php foreach($categories as $cat){ ?>
            <li><a href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a></li>
          <?php } ?>
          </ul>
          <?php endif; ?>
        </div>
        <div class="right-hold">
          <ul class="breadcrumbs">
            <?php
                $i = 1;
                foreach ($breadcrumbs as $breadcrumb)
                {
                    if ($i == count($breadcrumbs))
                    {
                        if (($breadcrumb['text'] == 'Статьи') or ($breadcrumb['text'] == 'articles'))
                            $breadcrumb['text'] = 'Статтi';
            ?>
                    <li class="active"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php
                    } else {
                        if (($breadcrumb['text'] == 'Статьи') or ($breadcrumb['text'] == 'articles'))
                            $breadcrumb['text'] = 'Статтi';
                    ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } 
                    $i++;
                }
            ?>
          </ul>
          <div class="content-holder">
            <div class="ttl"><?php echo $heading_title; ?></div>
            <div class="top-img">
              <img src="<?php echo $blog_image; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-responsive">
            </div>

            <?php echo $description; ?>
          </div>
        </div>
      </div>
<script type="text/javascript">
$(document).ready(function() {
	$('.popup_imgs').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.link2').addClass('active');
  })
</script>
<?php echo $footer; ?>
