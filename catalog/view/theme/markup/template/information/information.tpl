<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumbs">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php echo $description; ?>
      <?php
      if (is_array($information_files)) {
      ?>
      <div class="downloads-link">
        <ul>
          <?php
          foreach($information_files as $doc): ?>
          <?php if(1): ?>
          <li>
            <a href="/documents/<?php echo $doc['fileName']; ?>" target="_blank">
              <strong><?php echo $doc['fileTitle']; ?></strong>
              <?php
                  $name = $doc['fileName'];
                  $a = explode('.', $name);
                  ?>
              <span>Завантажити (<?php echo $a[1]; ?>)</span>
            </a>
          </li>
          <?php endif;
          ?>
          <?php endforeach;
 ?>

        </ul>
      </div>
<?php
}
?>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>