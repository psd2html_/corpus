<?php echo $header; ?>

<div class="baner">
<?php echo $content_top; ?>
        
      </div>
      <div class="holder-sidebar">
        <div class="sidebar">
          <div class="ttl">КАТАЛОГ ПРОДУКЦІЇ</div>
          <?php if($categories): ?>
          <ul>
            <?php foreach($categories as $cat): ?>
              <li><a href="<?php echo $cat['href'] ;?>"><?php echo $cat['name']; ?></a></li>
            <?php endforeach; ?>
            
          </ul>
        <?php endif; ?>
        </div>
        <div class="right-hold">
          <ul class="breadcrumbs">
          <?php 
$i = 1;
          foreach($breadcrumbs as $breadcrumb){ 
if($i == count($breadcrumbs)){
?>
<li class="active"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
<?php  
}
else{
          ?>
<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
          <?php 
}
$i++;
          } ?>  
            
          </ul>
          <div class="produkt-holder">
            <?php if($categories): ?>
            <ul>
              <?php foreach($categories as $cat): ?>
              <li <?php if($cat['icon'] == 1){ ?> class="ex" <?php } ?>>
                <a href="<?php echo $cat['href']; ?>">
                <div class="like_table"><div class="table_cell"> 
                  <?php if($cat['image'] != ''): ?>
                            <img src="/image/<?php echo $cat['image']; ?>" alt="<?php echo $cat['name']; ?>" class="img-responsive" style="max-height:100%;max-width: 190px;">
                        <?php else: ?>
                           <img src="/image/no_image.png" alt="<?php echo $cat['name']; ?>" class="img-responsive" style="max-height:100%;max-width: 190px;">
                        <?php endif; ?>
                        </div></div>
                  <span><?php echo $cat['name']; ?></span>
                </a>
              </li>
              <?php endforeach; ?>
              
            </ul>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <?php echo $content_bottom; ?>
      
<script type="text/javascript">
  $(document).ready(function(){
    $('.link1').addClass('active');
  })
</script>
<?php echo $footer; ?>