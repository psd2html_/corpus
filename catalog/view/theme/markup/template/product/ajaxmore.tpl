            <?php 

            if ($products) { ?>
              <div class="top-produkt top-produkt-more">
              
              <ul id="products_list">
                <?php foreach($products as $one): ?>
                  <?php //print_r($one); ?>
                <li class="<?php if($one['icon'] == 1){ ?>ex<?php } ?> hover">
                <div class="like_table"><div class="table_cell"> 
                  <a href="<?php echo $one['href']; ?>">
                  <img src="<?php echo $one['thumb']; ?>" alt="<?php echo $one['name']; ?>" style="display: block; max-width:100%; max-height:100%;margin:auto;">
                  </a>
                </div></div>    
                    <div class="bottom-inf">
                      <span><?php echo $one['name']; ?></span>
                      <div class="desc">
                        <?php echo $one['description']; ?>
                      </div>
                      <div>
                        <div class="price"><?php echo $one['price']; ?></div>
                        <a href="<?php echo $one['href']; ?>" class="btn-cut">Замовити</a>
                      </div>
                  </div>
                </li>
              <?php endforeach; ?>
              
                <?php if($show_more_button): ?>             
                <li class="btn-more-li">
                  <a href="javascript:void(0)" onclick="show.more(<?php echo $cat_id; ?>, <?php echo $page; ?>, <?php echo $limit; ?>, '<?php echo $path; ?>', <?php echo $already; ?>)" class="btn-more">ще</a>
                </li>
                </ul>
              <?php 
              else: ?>
              <?php endif; ?> 
</div>

              
<?php echo $pagination; ?>
              <?php } ?>