<?php echo $header; ?>
<style>
  #column-left{
    width:100%;
    float:none;
  }
</style>
<div class="holder-sidebar">
        <div class="sidebar">
          <?php if($categories): ?>
          <div class="ttl">КАТАЛОГ ПРОДУКЦІЇ</div>
          <ul>
            <?php foreach($categories as $cat): ?>
            <?php if($cat['id'] == $cat_id): ?>
            <li class="active">
            <a href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a>
            <div class="filter">
            <?php echo $column_left; ?>    
                
              </div>
            </li>
            <?php else: ?>
            <li><a href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a></li>
            <?php endif; ?>
            <?php endforeach; ?>
            
            
          </ul>
        <?php endif; ?>
        </div>
        <div class="right-hold">
          <div class="baner-02">
          <?php if($image): ?>
            <img src="<?php echo $image; ?>" alt="image" style="display: block; max-width:100%; max-height:100%;margin:auto;">
          <?php endif; ?>
          </div>
          <ul class="breadcrumbs">
            <?php 
$i = 1;
            foreach ($breadcrumbs as $breadcrumb) { 
if($i == count($breadcrumbs)){
?>
  <li class="active"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
<?php
}
else{
?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
<?php 
}
$i++;
            } 
?>
          </ul>
          <div class="txt-box">
            <div class="ttl"><?php echo $heading_title; ?></div>
            <?php echo $description; ?>
          </div>
          <div id="items_for">  
            <div class="top-produkt top-produkt-more">
              <?php if ($products) {  ?>
              <ul id="products_list">
                <?php foreach($products as $one): ?>
                <li <?php if($one['icon'] == 1){ ?>class="ex" <?php } ?>>
                  <a href="<?php echo $one['href'] ;?>">
<div class="like_table"><div class="table_cell"> 
                  <img src="<?php echo $one['thumb']; ?>" alt="<?php echo $one['name']; ?>" style="display: block; max-width:100%; max-height:100%;margin:auto;"></a>
</div></div>                  
                    <div class="bottom-inf">
                      <span><?php echo $one['name']; ?></span>
                      <div class="desc">
                        <?php echo $one['description']; ?>
                      </div>
                      <div>
                        <div class="price"><?php echo $one['uaprice']; ?></div>
                        <a href="<?php echo $one['href']; ?>" class="btn-cut">Замовити</a>
                      </div>
                  </div>
                </li>
              <?php endforeach; ?>
                <?php 
$mfp = isset($_REQUEST['mfp']);
                if(count($products) >= $page_limit and !$mfp): ?>
                <li class="btn-more-li">
                  <a href="javascript:void(0)" onclick="show.more(<?php echo $cat_id; ?>, <?php echo $current_page; ?>, <?php echo $page_limit; ?>, '<?php echo $path; ?>', <?php echo $page_limit; ?>)" class="btn-more">ще</a>
                </li>
              <?php endif; ?>
              </ul>
              <?php } ?>
            </div>
            
            <?php echo $pagination; ?>
            </div>
        </div>
      </div>

<script type="text/javascript">
  $(document).ready(function(){
    $('.link1').addClass('active');

  setTimeout(function(){
  $('.items-filter li input[disabled="disabled"]').each(function(){
//    alert();
    $(this).parent('div').addClass('disabled');
  });
  }, 500);
});
</script>
<?php echo $footer; ?>
