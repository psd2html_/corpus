<?php echo $header; ?>
<style>
  #column-left{
    width:100%;
    float:none;
  }
</style>
<div class="tovat-holder">
        <ul class="breadcrumbs">
        <?php
          $i = 1;
          foreach ($breadcrumbs as $breadcrumb) {
            if($i == count($breadcrumbs)){
        ?>
          <li class="active"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php
          }else{
        ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php }
          $i++;
        } ?>
        </ul>
        <div class="tovat-holder-in">
          <div class="main-desc">
            <div class="top-inf-holder">
              <div class="gallery">
                <div id="sync1" class="carousel-big">
					<a class="item" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
                  <?php if ($images) { ?>
                  <?php foreach ($images as $image) { ?>
				  <a class="item" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
                   <?php } ?>
                  <?php } ?>

                </div>
                <div id="sync2" class="carousel-smoll">
                  <div class="item"><img src="<?php echo $thumb; ?>" alt="image" class="center-block" style="display:block;max-width:100%;max-height:50px;"></div>
                  <?php if ($images) { ?>
                  <?php foreach ($images as $image) { ?>
                  <div class="item"><img src="<?php echo $image['thumb']; ?>" alt="image" class="center-block" style="display:block;max-width:100%;max-height:50px;"></div>
                   <?php } ?>
                  <?php } ?>

                </div>
              </div>
              <div class="text-inf">
                <div class="txt-height">
                  <div class="ttl"><?php echo $heading_title; ?></div>
                  <div class="txt-height-scroll">
                    <?php echo $description; ?>
                  </div>
                </div>
                <div class="price-box">
                  <div class="price-txt">
                      <?php
                      if($special):
                      echo $special;
                      else:
                      echo $price;
                      endif;
                      ?>
                  </div>
                  <div class="btn-hold" id="form">


                    <?php if ($quantity == 0): ?>
                    <span class="dop-inf"><?php echo 'Під замовлення'; //$stock; ?></span>
                    <?php else: ?>
                    <span class="dop-inf">В наявностi</span>
                    <?php endif; ?>

                    <input type="hidden" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
                    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                    <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn-01">
                    <?php echo $button_cart; ?>
                    </button>
                    <!--<a href="javascript:void(0);" onclick="cart.add(<?php echo $product_id; ?>, <?php echo $minimum; ?>); return false;" class="btn-01"><?php echo $button_cart; ?></a>-->
                  </div>
                </div>
              </div>
            </div>
            <div class="downloads-link">
              <ul>

              <?php foreach ($documents as $doc) : ?>
                <?php if ($doc['link'] != '') : ?>
                      <?php
                            $name = $doc['link'];
                            $a = explode('.', $name);
                      ?>
                    <li>
                      <a href="/documents/<?php echo $doc['link']; ?>" target="_blank">
                          <?php
                            if (isset($doc['title']) and !empty($doc['title']))
                                $_title = $doc['title'];
                            else
                                $_title = $a[0];
                          ?>
                        <strong><?php echo $_title; ?></strong>

                        <span>Завантажити (<?php echo $a[1]; ?>)</span>
                      </a>
                    </li>
                <?php endif; ?>
              <?php endforeach; ?>

              </ul>
            </div>
            <div class="table-inf">
            <?php if ($attribute_groups) { ?>
            <?php foreach($attribute_groups as $attribute_group){ ?>
              <ul>
              <?php foreach ($attribute_group['attribute'] as $attribute) { ?>

                  <li>
                  <div class="left"><?php echo $attribute['name']; ?></div>
                  <div class="right"><?php echo $attribute['text']; ?></div>
                </li>
                  <?php } ?>


              </ul>
              <?php } }?>
            </div>
          </div>
          <div class="add-prod">
          <?php if ($products) { ?>
            <div class="ttl-02">Аксесуари</div>
            <ul>
            <?php foreach($products as $product){ ?>
              <li>
                <div class="img"><div class="like_table"><div class="table_cell"> <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="image" class="img-responsive center-block" style="max-height:100%;"></a></div></div></div>
                <div class="txt">
                  <div class="name">
                    <strong><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></strong>
                    <span><?php echo $product['price']; ?></span>
                  </div>
                  <a href="javascript:void(cart.add('<?php echo $product['product_id']; ?>', '1'));" class="btn-add"></a>
                </div>
              </li>
              <?php } ?>

            </ul>
          <?php } ?>
          </div>
        </div>
      </div>
      <?php echo $column_left; ?>
<script type="text/javascript">
  $(document).ready(function(){
    $('.link1').addClass('active');
  })
</script>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {

	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
//		data: $('#form input[type=\'text\'], #form input[type=\'hidden\']'),
    data: {quantity : $('input[name="quantity"]').val(), product_id: $('input[name="product_id"]').val()},
		dataType: 'json',
		beforeSend: function() {
			//$('#button-cart').button('loading');
		},
		complete: function() {
			//$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');
      console.log(json);

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('#cart_count span').text(json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart_ul').load('index.php?route=common/cart/info ul li');
        $('.btn-all-cut').removeClass('hide');
        //console.log();
        $('#cart .all-price').text('Всього '+ json['summa']).removeClass('hide');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

//vova1381
$(document).ready(function() {
	$('.gallery').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>
