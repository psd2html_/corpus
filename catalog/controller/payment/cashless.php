<?php

//include_once(DIR_SYSTEM . 'library/fpdf/fpdf.php');
include_once(DIR_SYSTEM . 'library/fpdf/mc_table.php');
define('FPDF_FONTPATH', DIR_SYSTEM . 'library/fpdf/font/');

class ControllerPaymentCashless extends Controller {

    public function index() {
        $this->load->language('payment/cashless');

        $data['text_instruction'] = $this->language->get('text_instruction');
        $data['text_description'] = $this->language->get('text_description');
        $data['text_payment'] = $this->language->get('text_payment');
        $data['text_loading'] = $this->language->get('text_loading');

        $data['text_print_invoice_order'] = $this->language->get('text_print_invoice_order');

        $data['button_confirm'] = $this->language->get('button_confirm');

        $data['bank'] = nl2br($this->config->get('cashless_bank' . $this->config->get('config_language_id')));

        $data['continue'] = $this->url->link('checkout/success');
        //  $data['print'] = $this->url->link('payment/cashless/printpdf', 'token=' . $this->session->data['token'] . '&order_id=' . $this->session->data['order_id'], 'SSL');
        $data['print'] = $this->url->link('payment/cashless/printpdf', '&order_id=' . $this->session->data['order_id'], 'SSL');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/cashless.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/payment/cashless.tpl', $data);
        } else {
            return $this->load->view('default/template/payment/cashless.tpl', $data);
        }
    }

    public function confirm() {
        if ($this->session->data['payment_method']['code'] == 'cashless') {
            $this->load->language('payment/cashless');

            $this->load->model('checkout/order');

            $comment = $this->language->get('text_instruction') . "\n\n";
            $comment .= $this->config->get('cashless_bank' . $this->config->get('config_language_id')) . "\n\n";
            $comment .= $this->language->get('text_payment');

            $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('cashless_order_status_id'), $comment, true);
        }
    }

    public function printpdf() {

        $this->load->model('checkout/order');
        $this->load->model('account/customer');

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }

        $order_info = $this->model_checkout_order->getOrder($order_id);

        if ($order_info) {
            $customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);


            $payment_name = iconv("UTF-8", "CP1251", $order_info['payment_company'] . " " . $order_info['firstname'] . " " . $order_info['lastname']);


            $payment_address = iconv("UTF-8", "CP1251", $order_info['payment_postcode'] . ", " . $order_info['payment_country'] . ", " . $order_info['payment_zone'] . ", " . $order_info['payment_city'] . ", " . $order_info['payment_address_1'] . "");

            $pdf = new PDF_MC_Table();
            $pdf->AddFont('ArialMT', '', 'arialmt.php');
            $pdf->AddFont('Arial-BoldMT', '', 'arial_bold.php');
            $pdf->SetAuthor($order_info['store_name']);
            $pdf->SetTitle('Invoice');
            $pdf->SetTextColor(0, 10, 250);
            $pdf->AddPage();
            $pdf->SetDisplayMode("real", 'default');

            $pdf->SetFont('ArialMT', '', '7');
            $pdf->SetXY(10, 10);
            $pdf->SetDrawColor(0, 10, 250);
            $pdf->Cell(190, 13, '', 1, 0, 'C', 0);

            $pdf->SetXY(15, 12);
            $pdf->Write(7, iconv("UTF-8", "CP1251", "Увага! Оплата цього рахунку означає погодження з умовами поставки товарів. Повідомлення про оплату є обов'язковим, в іншому випадку не гарантується"));
            $pdf->SetXY(15, 15);
            $pdf->Write(7, iconv("UTF-8", "CP1251", " наявність товарів на складі. Товар відпускається за фактом надходження коштів на п/р Постачальника, самовивозом, за наявності довіреності та паспорта."));

            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Arial-BoldMT', '', '9');
            $pdf->SetXY(70, 32);
            $pdf->Cell(190, 8, iconv("UTF-8", "CP1251", "Зразок заповнення платіжного доручення"), 0, 0, 'L', 0);

            $pdf->SetXY(10, 33); // основаня рамка
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->Cell(190, 35, '', 1, 1, 'C', 0);

            $pdf->SetFont('ArialMT', '', '7');
            $pdf->SetXY(11, 37);
            $pdf->Cell(120, 8, iconv("UTF-8", "CP1251", 'Одержувач:'), 0, 0, 'L', 0);

            // Одержувач
            $user_name = iconv("UTF-8", "CP1251", html_entity_decode($this->config->get('cashless_beneficiary')));
            $pdf->SetFont('Arial-BoldMT', '', '9'); // Получатель платежа
            $pdf->SetXY(11, 42);
            $pdf->Cell(120, 7, $user_name, 0, 0, 'L', 0);


            $user_bin = $this->config->get('cashless_bin');
            $pdf->SetFont('ArialMT', '', '9'); // КОД
            $pdf->SetXY(11, 49);
            $pdf->Cell(120, 8, iconv("UTF-8", "CP1251", 'Код: '), 0, 0, 'L', 0);

            $pdf->SetXY(30, 49);
            $pdf->Cell(120, 8, iconv("UTF-8", "CP1251", $user_bin), 0, 0, 'L', 0);

            $pdf->SetXY(23, 49); // Рамка Код
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->Cell(30, 8, '', 1, 1, 'C', 0);

            $pdf->SetFont('ArialMT', '', '7');
            $pdf->SetXY(11, 56);
            $pdf->Cell(34, 8, iconv("UTF-8", "CP1251", 'Банк одержувача: '), 0, 0, 'L', 0);

            $pdf->SetFont('Arial-BoldMT', '', '9'); // Банк получателя платежа
            $pdf->SetXY(11, 60);
            $pdf->Cell(120, 8, iconv("UTF-8", "CP1251", html_entity_decode($this->config->get('cashless_bankname'))), 0, 0, 'L', 0);

            $pdf->SetXY(100, 58);
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->Cell(20, 10, '', 1, 1, 'C', 0);

            $pdf->SetFont('Arial-BoldMT', '', '8'); // Код банку
            $pdf->SetXY(95, 57);
            $pdf->Cell(30, 7, iconv("UTF-8", "CP1251", 'Код банку'), 0, 0, 'C', 0);

            $pdf->SetFont('Arial-BoldMT', '', '9'); // Код банку
            $pdf->SetXY(95, 62);
            $pdf->Cell(30, 7, iconv("UTF-8", "CP1251", $this->config->get('cashless_bik')), 0, 0, 'C', 0);

            //КРЕДИТ рах. N
            $pdf->SetXY(125, 50);
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->Cell(40, 8, '', 1, 1, 'C', 0);

            $pdf->SetFont('Arial-BoldMT', '', '8');  // КНП
            $pdf->SetXY(125, 44);
            $pdf->Cell(40, 7, iconv("UTF-8", "CP1251", 'КРЕДИТ рах. N'), 0, 0, 'C', 0);

            $pdf->SetFont('Arial-BoldMT', '', '10'); // КНП
            $pdf->SetXY(125, 51);
            $pdf->Cell(40, 7, iconv("UTF-8", "CP1251", $this->config->get('cashless_knp')), 0, 0, 'C', 0);



            $pdf->SetXY(125, 58);
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->Cell(40, 10, '', 1, 1, 'C', 0);

            // Номер договора
            $pdf->SetXY(10, 73);
            $pdf->SetFontSize(13);
            $order_date = date("d-m-Y", strtotime($order_info['date_modified']));  // Дата договора
            //$pdf->Write(5, iconv("UTF-8", "CP1251", 'Рахунок на оплату № ' . $order_info['invoice_prefix'] . $order_id . " від " . $order_date . "г."));
            $pdf->Write(5, iconv("UTF-8", "CP1251", 'Рахунок на оплату № ' . $order_id . " від " . $order_date . "г."));

            $pdf->SetXY(10, 78);
            $pdf->Cell(190, 0.2, '', 1, 0, 'L', 1);
            $pdf->SetFont('ArialMT', '', '8');
            $pdf->SetXY(11, 84);
            $pdf->Cell(39, 7, iconv("UTF-8", "CP1251", 'Постачальник:'), 0, 0, 'L', 0);

            $pdf->SetFont('Arial-BoldMT', '', '8');    // Имя
            $pdf->SetXY(50, 80);
            $pdf->Cell(150, 7, $user_name, 0, 0, 'L', 0);

            $pdf->SetFont('ArialMT', '', '8');    // Поставщик
            $pdf->SetXY(50, 84);
            $pdf->Cell(150, 7, iconv("UTF-8", "CP1251", 'П/р ' . $this->config->get('cashless_knp') . ', ') . iconv("UTF-8", "CP1251", html_entity_decode($this->config->get('cashless_bankname'))) . iconv("UTF-8", "CP1251", ', МФО ' . $this->config->get('cashless_bik')), 0, 0, 'L', 0);

            $pdf->SetFont('ArialMT', '', '8');    // Юридический адрес
            $pdf->SetXY(50, 88);
            $pdf->Cell(150, 7, iconv("UTF-8", "CP1251", $this->config->get('cashless_address')), 0, 0, 'L', 0);

            $pdf->SetFont('ArialMT', '', '8');    // Телефон/факс продавца
            $pdf->SetXY(50, 92);
            $pdf->Cell(150, 7, iconv("UTF-8", "CP1251", 'код ЄДРПОУ ' . $user_bin . ', ІПН 395789509159'), 0, 0, 'L', 0);



            $pdf->SetFont('ArialMT', '', '8');
            $pdf->SetXY(11, 98);
            $pdf->Cell(39, 7, iconv("UTF-8", "CP1251", 'Покупець:'), 0, 0, 'L', 0);

            $pdf->SetFont('Arial-BoldMT', '', '8');
            $pdf->SetXY(50, 98);
            $pdf->Cell(150, 7, '' . str_replace('&quot;', '"', $payment_name) . '', 0, 0, 'L', 0);

            $pdf->SetFont('Arial-BoldMT', '', '8');
            $pdf->SetXY(50, 102);
            $pdf->Cell(150, 7, $payment_address, 0, 0, 'L', 0);

            $pdf->SetFont('Arial-BoldMT', '', '8');
            $pdf->SetXY(50, 106);
            $pdf->Cell(150, 7, iconv("UTF-8", "CP1251", $order_info['telephone'] . ', ' . $order_info['email']), 0, 0, 'L', 0);

            //$pdf->SetFont('ArialMT', '', '8');
            //$pdf->SetXY(11, 110);
            //$pdf->Cell(39, 7, iconv("UTF-8", "CP1251", 'Договір (Замовлення):'), 0, 0, 'L', 0);

            $pdf->SetFont('Arial-BoldMT', '', '8');    // № заказа
            $pdf->SetXY(50, 110);
            $pdf->Cell(150, 7, iconv("UTF-8", "CP1251", ' № ' . $order_id . ' від ' . $order_date . 'г.'), 0, 0, 'L', 0);


            $pdf->SetXY(10, 116);
            $pdf->Cell(190, 0.2, '', 1, 0, 'L', 1);

            //=============================================================================================================
            //table start
            //title start
            //Set widths
            $pdf->SetWidths([6,27,102,12,9,17,17]);
            //Set fonts
            $pdf->SetFont('Arial-BoldMT', '', '7.5');
            //Set coordinates
            $pdf->SetXY(10, 120);
            //Set alight
            $pdf->SetAligns(['C','C','C','C','C','C','C']);
            $pdf->Row([
                iconv("UTF-8", "CP1251", '№'),
                iconv("UTF-8", "CP1251", 'Артикул'),
                iconv("UTF-8", "CP1251", 'Товари (роботи, послуги)'),
                iconv("UTF-8", "CP1251", 'Кіль-ть'),
                iconv("UTF-8", "CP1251", 'Ед.'),
                iconv("UTF-8", "CP1251", 'Ціна без ПДВ'),
                iconv("UTF-8", "CP1251", 'Сума без ПДВ')
            ]); //Set table header
            //Set fonts
            $pdf->SetFont('ArialMT', '', '7.5');
            /** @var array $products Get all products by order_id*/
            $products = $this->getOrderProducts($order_id);
            /** @var int $i count rows, default 1*/
            $i = 1;
            foreach ($products as $product) {
                //Set alight
                $pdf->SetAligns(['C','L','L','C','C','R','R']);
                //Render Row
                $pdf->Row([
                    $i,
                    iconv("UTF-8", "CP1251", str_replace('&quot;', '"', $product['name'])),
                    iconv("UTF-8", "CP1251", str_replace('&quot;', '"', $product['cart_description'])),
                    iconv("UTF-8", "CP1251", $product['quantity']), iconv("UTF-8", "CP1251", 'шт.'),
                    iconv("UTF-8", "CP1251", $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value'], false)),
                    iconv("UTF-8", "CP1251", $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value'], false))
            ]);
                $i++;
            }

            // TODO fast fix
            // Get Y after generate code.
            // $pdf->GetY();
            if($i <= 4){
                $k =12;
            }elseif($i >= 8){
                $k = 9;
            }else{
                $k = 12;
            }

            $y_offset = $k * $i;
            //=============================================================================================================
            $pdf->SetXY(10, 130 + $y_offset);
            $pdf->Cell(190, 0.2, '', 1, 0, 'L', 1);

            $totals = $this->getOrderTotals($order_id);
            $pdf->SetFont('Arial-BoldMT', '', '8');

            foreach ($totals as $total) {
                $y_offset = $y_offset + 4;

                $pdf->SetXY(10, 130 + $y_offset);
                $pdf->Cell(190, 6, iconv("UTF-8", "CP1251", $total['title'] . ': ' . $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value'], false)), 0, 0, 'R', 0);
            }

            $pdf->SetFont('ArialMT', '', '8');
            $pdf->SetXY(10, 134 + $y_offset);
            $pdf->Cell(190, 7, iconv("UTF-8", "CP1251", 'Всього найменувань ' . ($i - 1) . ', на суму ' . $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false) . ' грн.'), 0, 0, 'L', 0);

            $pdf->SetXY(10, 140 + $y_offset);

            // Сумма прописью
            $total_signs = $this->num2str($order_info['total']);
            $pdf->Cell(190, 6, iconv("UTF-8", "CP1251", 'Всьго до сплати: ' . $total_signs), 0, 0, 'L', 0);
            $pdf->SetXY(10, 145 + $y_offset);
            $total_tax = $this->num2str($totals[1]['value']);
            $pdf->Cell(190, 6, iconv("UTF-8", "CP1251", 'У т.ч. ПДВ: ' . $total_tax), 0, 0, 'L', 0);
            //Line
            $pdf->SetXY(10, 152 + $y_offset);
            $pdf->Cell(190, 0.2, '', 1, 0, 'L', 1);

            //If page end, create new page and put this part there.
            if (150 + $y_offset >= 270){
                $pdf->CheckPageBreak(270);
                //Last, order holder
                $pdf->SetXY(10, 10);
                $pdf->Cell(190, 6, iconv("UTF-8", "CP1251", 'Виписав(ла): '), 0, 0, 'L', 0);
                $pdf->SetXY(30, 16);
                $pdf->Cell(120, 0.1, '', 1, 0, 'L', 1);
                $pdf->SetXY(60, 11);
                $pdf->Cell(120, 6, iconv("UTF-8", "CP1251", 'Мірошніченко Наталя Миколаївна'), 0, 0, 'L', 0);
            }else{
                //Last, order holder
                $pdf->SetXY(10, 155 + $y_offset);
                $pdf->Cell(190, 6, iconv("UTF-8", "CP1251", 'Виписав(ла): '), 0, 0, 'L', 0);
                $pdf->SetXY(30, 161 + $y_offset);
                $pdf->Cell(120, 0.1, '', 1, 0, 'L', 1);
                $pdf->SetXY(60, 156 + $y_offset);
                $pdf->Cell(120, 6, iconv("UTF-8", "CP1251", 'Мірошніченко Наталя Миколаївна'), 0, 0, 'L', 0);
            }


            $pdf->Output('invoice_N' . $order_id . '_' . str_replace('.', '', $order_date) . '.pdf', 'I');
        } else {
            echo "<font face=\"Arial,Helvetica,Tahoma\" color=\"red\" size=\"2\">Ошибка: Такого Заказа нет</font><br>";
        }
    }

    private function getOrderProducts($order_id) {
        $query = $this->db->query("SELECT op.*, p.cart_description FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "product p ON (op.product_id = p.product_id) WHERE op.order_id = '" . (int) $order_id . "'");

        return $query->rows;
    }

    private function getOrderTotals($order_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int) $order_id . "' ORDER BY sort_order");

        $data = array();
        foreach ($query->rows as $row) {
            $name = '';
            if ($row["code"] == "sub_total") $name = "Разом";
            if ($row["code"] == "tax") $name = "Сума ПДВ";
            if ($row["code"] == "total") $name = "Усього з ПДВ";
            $data[] = array(
                'order_total_id' => $row["order_total_id"],
                'order_id' => $row["order_id"],
                'code' => $row["code"],
                'title' => ($name != '') ? $name : $row["title"],
                'value' => $row["value"],
                'sort_order' => $row["sort_order"],
            );
        }
        return $data;
    }

    private function getCustomFieldDescriptions($custom_field_id) {

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "custom_field_description WHERE custom_field_id = '" . (int) $custom_field_id . "' AND language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    /**
     * Number to Ukrainian string
     * @param float $num
     *
     * @return string
     */
    private function num2str($num) {
        $nul = "нуль";
        $ten = [
            ['', 'одна', 'дві', 'три', 'чотири', 'п\'ять', 'шість', 'сім', 'вісім', 'дев\'ять',],
            ['', 'одна', 'дві', 'три', 'чотири', 'п\'ять', 'шість', 'сім', 'вісім', 'дев\'ять',]
        ];
        $a20 = ["десять", "одинадцять", "дванадцять", "тринадцять", "чотирнадцять", "п'ятнадцять", "шістнадцять", "сімнадцять", "вісімнадцять", "дев'ятнадцять"];
        $tens = [2 => "двадцять", "тридцять", "сорок", "п'ятдесят", "шістдесят", "сімдесят", "вісімдесят", "дев'яносто"];
        $hundred = ["", "сто", "двісті", "триста", "чотириста", "п'ятсот", "шістсот", "сімсот", "вісімсот", "дев'ятсот"];
        $unit = [
            ["копійок", "копійок", "копійок", 1],
            ["гривень", "гривні", "гривень", 0],
            ["тисяча", "тисячі", "тисяч", 1],
            ["мільйон", "мільйони", "мільйонів", 0],
            ["мільярд", "міліарда", "мільярдів", 0],
        ];

        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v))
                    continue;
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1)
                    $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3];# 20-99
                else
                    $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3];# 10-19 | 1-9
                // units without rub & kop
                if ($uk > 1)
                    $out[] = $this->morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
            } //foreach
        } else
            $out[] = $nul;
        $out[] = $this->morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        $out[] = $kop . ' ' . $this->morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    private function morph($n, $f1, $f2, $f5) {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20)
            return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5)
            return $f2;
        if ($n == 1)
            return $f1;
        return $f5;
    }

}
