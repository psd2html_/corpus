<?php
class ControllerProductShowall extends Controller {

  public function index() {

      $this->load->model('catalog/category');

      $this->load->model('catalog/product');

      $this->load->model('tool/image');

      $this->load->language('product/category');

      $this->document->setTitle('Продукція');
      $this->document->setDescription('Продукція');
      $this->document->setKeywords('Продукція');

      $data = array();

      //breadcrumbs
      $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/home')
    );
    $data['breadcrumbs'][] = array(
      'text' => 'Каталог продукція',
      'href' => $this->url->link('product/showall')
    );

    // Categories

    $data['categories'] = array();

    $categories = $this->model_catalog_category->getCategories(0);

    foreach ($categories as $category) {
      
      //if ($category['top']) {
        // Level 2
        $children_data = array();

        $children = $this->model_catalog_category->getCategories($category['category_id']);

        foreach ($children as $child) {
          $filter_data = array(
            'filter_category_id'  => $child['category_id'],
            'filter_sub_category' => true
          );

          $children_data[] = array(
            'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
            'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
            'image' => $child['image']
          );
        }

        // Level 1
        $data['categories'][] = array(
          'name'     => $category['name'],
          'children' => $children_data,
          'column'   => $category['column'] ? $category['column'] : 1,
          'href'     => $this->url->link('product/category', 'path=' . $category['category_id']),
          'image'    => $category['image'],
          'icon'     => $category['icon']
        );
      //}
    }



      //page parts
      $data['footer'] = $this->load->controller('common/footer');
      $data['header'] = $this->load->controller('common/header');
      $data['content_top'] = $this->load->controller('common/content_top');
      $data['content_bottom'] = $this->load->controller('common/content_bottom');

      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/showall.tpl')) {
        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/showall.tpl', $data));
      } else {
        $this->response->setOutput($this->load->view('default/template/product/showall.tpl', $data));
      }
  }
}