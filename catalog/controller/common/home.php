<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		//$data['header_top'] = $this->load->controller('common/content_slog');

		
		// Categories
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			
			//if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
						'image' => $child['image']
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id']),
					'image'		 => $category['image'],
					'icon'		 => $category['icon']
				);
			//}
		}

		//last posts
		$this->load->model('tltblog/tltblog');
		$this->load->model('setting/setting');
		$this->load->model('tool/image');
		
		$data['posts'] = array();
		$posts = $this->model_tltblog_tltblog->getTltBlogs();
		
				if (isset($this->request->get['tltpath'])) {
					$path = $this->request->get['tltpath'];
				} elseif ($this->config->has('tltblog_path')) {
					$path = $this->config->get('tltblog_path');
				} else {
					$path = 'blogs';
				}
		$res = array();
		foreach($posts as $post){
			$post['href'] = $this->url->link('tltblog/tltblog', 'tltpath=' . $path . '&tltblog_id=' . $post['tltblog_id']);
			//$post['intro'] = substr(strip_tags(html_entity_decode($post['intro'], ENT_QUOTES, 'UTF-8')), 0, 220);
			$post['intro'] = limit_text(strip_tags(html_entity_decode($post['intro'], ENT_QUOTES, 'UTF-8')), 40);
			array_push($data['posts'], $post);
		}


		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}

	}
}

		function limit_text($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
    }