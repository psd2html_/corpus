ALTER TABLE `oc_tltblog` ADD `show_title` TINYINT(1)  NOT NULL  DEFAULT '0'  AFTER `show_description`;

UPDATE `oc_tltblog` SET `show_title` = '1';
