<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<!--<script src="catalog/view/theme/markup/markup/js/jquery-1.10.2.min.js" type="text/javascript"></script>-->
<link href="catalog/view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen" />
				
				<script src="catalog/view/javascript/mf/jquery-ui.min.js" type="text/javascript"></script>
			
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/markup/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/markup/markup/css/styles.css" media="all" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/markup/markup/css/nouislider.css" media="all" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/markup/markup/css/owl.carousel.css" media="all" />

<link rel="stylesheet" type="text/css" href="catalog/view/theme/markup/markup/css/jquery.mCustomScrollbar.css" media="all" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/markup/markup/css/slicknav.css" media="all" />
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<script type="text/javascript" src="catalog/view/theme/markup/markup/js/jquery.screwdefaultbuttonsV2.js"></script>
<script type="text/javascript" src="catalog/view/theme/markup/markup/js/nouislider.js"></script>

<script type="text/javascript" src="catalog/view/theme/markup/markup/js/owl.carousel.js"></script>

<script type="text/javascript" src="catalog/view/theme/markup/markup/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="catalog/view/theme/markup/markup/js/jquery.slicknav.js"></script>
<script type="text/javascript" src="catalog/view/theme/markup/markup/js/scripts.js"></script>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
<link rel="shortcut icon" href="catalog/view/theme/markup/markup/images/favicon.ico" type="image/x-icon">
<style>
  form .custom-field{
    display:none;
  }
</style>
</head>
<body class="<?php echo $class; ?>">

<div id="header">
  <div class="container">
    <div class="container-in">
      <nav class="main-menu">
        <ul>
          <li class="link1"><a href="<?php echo $showall; ?>">Продукція</a></li>
          <li class="link2"><a href="<?php echo $articles_link; ?>">Статті</a></li>
          <li class="link3"><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
        </ul>
      </nav><!-- / nav -->
      <div class="right-hold">
        <div class="search-box">
          <i class="ico-s"></i>
          <div class="drop-s">
            <?php echo $search; ?>
          </div>
        </div>
        <?php if (!$logged) { ?>
        <ul>
          <li><a href="#" class="for-login"><?php echo $text_login; ?></a></li>
          <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
        </ul>
        <div class="reg-aft-form" style="width:360px;">
          <div class="ttl">Будь ласка, авторизуйтесь</div>
          <div class="form-reg-in">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
              <fieldset>
                <div class="row">
                  <input type="email" class="txt" name="email" placeholder="Email">
                  <input type="password" class="txt" name="password" placeholder="Пароль">
                </div>
                <div class="row">
                  <div class="r-pass">
                    <input type="checkbox" id="forget-pass" class="check">
                    <label for="forget-pass">Запам'ятати мене</label>
                  </div>
                  <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                </div>
                <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
                <div class="row"><input type="submit" class="btn-02" value="Авторизація"></div>
              </fieldset>
            </form>
          </div>
          <div class="botom-inf">
            <strong>Або ви новий користувач?</strong>
            <p>Створити аккаунт, щоб відстежувати<br/>ваші замовлення</p>
            <a href="<?php echo $register; ?>" class="btn-02">Створити аккаунт</a>
          </div>
        </div>
        <?php } else { ?>
      
        <div class="user">
          <span class="name"><?php echo $user_name; ?></span>
          <div class="drop-list">
            <ul>
              <li class="prof">
                <a href="/my-account">
                  <div>Мій профіль</div><span>ред.</span>
                </a>
              </li>
              <li class="order">
                <a href="/order-history">Стан замовлення</a>
              </li>
              <li class="exit">
                <a href="/logout">Вихід</a>
              </li>
            </ul>
          </div>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>  
</div>
<div class="main-hold">
  <div class="container">
    <div class="container-in">
      <div class="top-inf">
        <strong class="logo"><a href="<?php echo $home; ?>">Corpus</a></strong>
        <!---->
          <?php echo $cart; ?>
          <div class="tel-holder">
            
<?php echo $header_phones; ?>
          </div>
          <div class="slogan">
<?php echo $header_top; ?>
          <!--слоган/ключевая фраза-->
          </div>
          
            
          
      </div>
        
      </div>
    
<?php if (!$categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>
<?php } ?>
